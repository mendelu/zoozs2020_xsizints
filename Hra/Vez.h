//
// Created by 79242 on 15.12.2020.
//

#ifndef HRDINA_CPP_VEZ_H
#define HRDINA_CPP_VEZ_H
#include <iostream>
#include "Patro.cpp"
#include <array>
#include "Nepritel.cpp"

class Vez{
    std::array<Patro*, 3> m_patro;
    std::array<Nepritel*, 5> m_nepritel;
public:
    Nepritel* getNep(int i);
    int patraVeze(int inp);
    void novyNepritel(int inp);



};






#endif //HRDINA_CPP_VEZ_H
