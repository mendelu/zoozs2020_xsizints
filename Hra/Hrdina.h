//
// Created by 79242 on 12.12.2020.
//

#ifndef PROJEKT_HRDINA_H
#define PROJEKT_HRDINA_H

#include <stdlib.h>
#include <iostream>



class Hrdina {
    std::string m_jmeno;
    std::string m_povolani;
    int m_hp;
    int m_sila;
    int m_obrana;
    int m_magie;

public:
    Hrdina(std::string jmeno, int  hp, int sila, int obrana,int magie,std::string povolani );
   // Hrdina();

    void setObrana(int inp);
    void printInfo();
    int getZdravi();
    int getSila();

};





#endif //PROJEKT_HRDINA_H
