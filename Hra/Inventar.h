//
// Created by 79242 on 12.12.2020.
//

#ifndef PROJEKT_INVENTAR_H
#define PROJEKT_INVENTAR_H

#include <array>
#include "Predmet.h"
#include"PrazdnySlot.h"
#include "Brneni.h"
#include "Zbran.h"

class Inventar{
    std::array<Predmet*, 7> m_predmety;
    int pocPred;
public:
    void novyInventar();
    void spravaInventare(int inp);
    void pridejPredmet(Predmet* predmet);
    void vypisInventare();
    void odeberPredmet(int pozice);
};


#endif //PROJEKT_INVENTAR_H
