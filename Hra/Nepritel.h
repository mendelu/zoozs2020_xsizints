//
// Created by 79242 on 15.12.2020.
//

#ifndef HRDINA_CPP_NEPRITEL_H
#define HRDINA_CPP_NEPRITEL_H
#include <iostream>

class Nepritel{
protected:
    std::string m_jmeno;
    int m_sila;
    int m_hp;
    bool m_stav;

public:
    Nepritel(std::string jmeno, int sila, int hp);

    int getSilaUtoku();
    int getHp();
    bool getStav();
    void setHp(int inp);
    bool setStav();
    std::string  getJmeno();
};













#endif //HRDINA_CPP_NEPRITEL_H
