//
// Created by 79242 on 12.12.2020.
//

#ifndef PROJEKT_ZBRAN_H
#define PROJEKT_ZBRAN_H

#include <iostream>
#include "Predmet.h"

class Zbran: public Predmet {
    std::string m_jmeno;
    int m_bonus;
public:
    Zbran(std::string jmeno,int bonus);

    int getBonusUtoku();

   void pouzitPredmet();
    void printInfo();
};









#endif //PROJEKT_ZBRAN_H
