//
// Created by 79242 on 12.12.2020.
//

#ifndef PROJEKT_PREDMET_H
#define PROJEKT_PREDMET_H

#include <stdlib.h>
#include <iostream>


class Predmet{
protected:
    std::string m_jmeno;
    int m_bonus;

public:
    Predmet(std::string jmeno,int bonus);
    Predmet(std::string jmeno);
    std::string printJmeno();

   // virtual void pouzitPredmet()=0;
    virtual void printInfo() = 0;

};









#endif //PROJEKT_PREDMET_H
