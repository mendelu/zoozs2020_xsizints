//
// Created by 79242 on 12.12.2020.
//

#ifndef PROJEKT_BRNENI_H
#define PROJEKT_BRNENI_H

#include "Predmet.h"
#include "Hrdina.h"

class Brneni: public Predmet {
    std::string m_jmeno;
    int m_bonus;
public:
    Brneni(std::string jmeno, int bonus);

    float getBonusObrany();
    std::string getJmeno();
  //  void pouzitPredmet(Hrdina* a);
    void printInfo();

};

#endif //PROJEKT_BRNENI_H
