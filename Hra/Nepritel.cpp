//
// Created by 79242 on 15.12.2020.
//

#include "Nepritel.h"

Nepritel::Nepritel(std::string jmeno, int sila, int hp ){
    m_jmeno = jmeno;
    m_sila = sila;
    m_hp = hp;
    m_stav = true;
}

std::string Nepritel::getJmeno(){return m_jmeno;}
int Nepritel::getSilaUtoku(){return m_sila;}
int Nepritel::getHp(){return m_hp;}
bool Nepritel::getStav(){ return  m_stav;}
void Nepritel::setHp(int inp){m_hp = inp; }
bool Nepritel::setStav(){m_stav = false; }

