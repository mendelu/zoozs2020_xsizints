//
// Created by 79242 on 13.12.2020.
//

#include "Brneni.h"

Brneni::Brneni(std::string jmeno, int bonus):Predmet(jmeno,bonus){
    m_jmeno = jmeno;
    m_bonus = bonus;
}

float Brneni::getBonusObrany() {
    return m_bonus;
}

//void Brneni::pouzitPredmet(Hrdina* a){
  //  a->setObraba(m_bonus);
//}

void Brneni::printInfo() {
    std::cout << std::endl;
    std::cout << "Jmeno: "<< m_jmeno << std::endl;
    std::cout << "Bonus k obrane: " << getBonusObrany() << std::endl;
}

