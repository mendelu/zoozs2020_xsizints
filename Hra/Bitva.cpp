//
// Created by 79242 on 16.12.2020.
//
#include "Bitva.h"
//#include "Hra.cpp"
#include "Hrdina.cpp"
#include "Vez.cpp"
#include "Inventar.cpp"

void bitva(){
    for (int i=0;i < 5; i++){
        if  (Vez1->getNep(i)->getStav() == true){
            std::cout << "Rvacka zacals se" << std::endl;
            while (Vez1->getNep(i)->getHp()>0 and karel->getZdravi()>0 ){
                akceBitve(Vez1->getNep(i));
                

            }
        }
    }
}

void akceBitve(Nepritel* nep){
    std::cout << "Vyberte akce" << std::endl;
    std::cout << "1. Utok" << std::endl;
    std::cout << "2. Otevrit inventar" << std::endl;
    int inp;
    std::cin >> inp;
    switch (inp) {
        case 1:{
            std::cout << "Jste vyrisili utocit na " << nep->getJmeno() <<std::endl;
            nep->setHp(nep->getHp() - karel->getSila());
            std::cout << "U " << nep->getJmeno() << " zustalo se "<< nep->getHp() << " zivotu" <<std::endl;
            return smrt(nep);
        }
        case 2:{
            std::cout << "Jste vyrisili otevrit inventar" <<std::endl;
            newInv->vypisInventare();
            return smrt(nep);
        }
    }
}

void smrt(Nepritel* nep){
    if (nep->getHp()<=0){
        nep->setStav();
        return bitva();
    }
}