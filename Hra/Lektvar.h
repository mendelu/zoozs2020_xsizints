//
// Created by 79242 on 12.12.2020.
//

#ifndef PROJEKT_LEKTVAR_H
#define PROJEKT_LEKTVAR_H

#include "Predmet.h"

class Lektvar: public Predmet{

public:
    Lektvar(std::string jmeno,float bonus);

    float getBonusKZivotum();

    void pouzitPredmet();
    void printInfo();
};







#endif //PROJEKT_LEKTVAR_H
